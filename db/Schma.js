const mongoose = require('mongoose');
const crypto = require('crypto'); // хз, зачем это
const generator = require('generate-password'); // для генерации строки
var validator = require('validator'); // для проверки почты и телефона
var nodemailer = require('nodemailer'); // для отправки пароля на почту
var moment = require('moment'); // для указания даты

//Проверит поля на то что они номальные тайт ограниеченное число символов напрмиер, почту провить на наличие "@" в адресе и тд, если поле не соотвествует то вывести ошибку
const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:new Date()
    },
    title:{
        type:String,
        required:true
    },
    text:{
        type:String,
        required:true
    },
    timeLost:{
        type:String,
        required:true
    },
    contacts:{
        type:String,
        required:true
    },
    type:{
        type:String,
        required:true
    },
    password:{
        type:String,
        default:"saaffa"
    },
    typeCorrect:{
        type:Boolean,
        default:false
    },
},{
    collection: "lists",
    versionKey: false
});

userSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    
    next();
});

module.exports = mongoose.model('lists', userSchema);

const adSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    typeCorrect: {
        type: Boolean,
        default: false
    }, 
    date: {
        created_at: Date,
        updated_at: Date
    },
    title: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: [true, 'Пожалуйста, введите почту'],
        validate: {
            validator: function(text) {
                return validator.isEmail(text);
            },
            message: 'Не правильно введена почта'
        }
    },
    key: String,
    //img: Buffer,
    timeLost: String, 
    placeLost: String, 
    contacts: {
        type: [String],
        validate: {
            validator: function(text) {
                return text.length <= 4;
            },
            message: 'Вы не можете ввести больше 4-х контактов'
        }
    },
    type: {
        type: String,
        enum : ['lost','found', 'archive'],
        default: 'lost'
    },
    cron_date: Date
},{
    collection: "Ad",
    versionKey: false
});

adSchema.pre('save', function(next) {

    var currentDate = new Date(); // получение текущей даты
    var password = generator.generate({ // генерация ключа
        length: 10,
        numbers: true
    });
    var cronDate = moment().add(30, 'day'); // дата через 30 дней

    //console.log(this.cron_date.getMinutes() + " " + this.cron_date.getHours() + " " + this.cron_date.getDate() + " " + (this.cron_date.getMonth()+1));

    var transporter = nodemailer.createTransport({ // данные почтового ящика отправителя
        service: 'gmail',
        auth: {
            user: 'searchthingsmessages@gmail.com',
            pass: 'l8xvWS7LR6'
        }
    });

    if (!this.typeCorrect) // если объявление не одобрено
    {
        var mailOptions = { // содержание сообщения
            from: 'searchthingsmessages@gmail.com',
            to: this.email,
            subject: 'Ваше объявление "' + this.title + '" поступило в обработку',
            text: 'Объявление появиться на сайте сразу после проверки. Для внесения изменений в объявление вам понадобиться ввести этот пароль: ' + password 
        }
    }
    else if (this.typeCorrect) // если объявление одобрено
    {
        var mailOptions = { // содержание сообщения
            from: 'searchthingsmessages@gmail.com',
            to: this.email,
            subject: 'Ваше объявление "' + this.title + '" успешно опубликовано',
            text: 'Объявление прошло проверку, вы можете просмотреть его на нашем замечательном сайте. Для внесения изменений в объявление вам понадобиться ввести этот пароль: ' + password
        }
    }     

    transporter.sendMail(mailOptions, function(error, info){ // отправка сообщения с паролем
    if (error) {
        console.log(error);
    } else {
        console.log('Сообщение успешно отправлено: ' + info.response);
    }
    });
    
    if(this._id===null||this._id===undefined) this._id = new mongoose.Types.ObjectId();
    
    this.date.updated_at = currentDate; // изменение значения при редактировании
    this.cron_date = cronDate; // срок публикации записи
  
    if (!this.date.created_at) // если это новое объявление
    {
        this.date.created_at = currentDate;
        this.key = password;
    } 
  
    next();
});

module.exports = mongoose.model('Ad', adSchema);