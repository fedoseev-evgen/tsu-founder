const Ad = require('./../../db/Schma');

module.exports = function (req, res) {
   Ad.deleteOne({ _id: req.body.id }, (err) => {
      if (err)// вывод сообщения об ошибке
         res.send(`Запись с id ${req.body.id} не найдена!`);
      else
         res.send(`Запись ${req.body.id} была успешно удалена!`);
   });
}