const Ad = require('./../../db/Schma');

module.exports = function (req, res) {
   //Тут должна редактироваться запись, с фронта приходит вся ее инфа, а ваша задача перезаписать запись с помощью update
   //Вы получите id и password, надо проверить их соответствие и перезаписать если нашло
   Ad.findById({ _id: req.body.id }, (err, results) => {
      if (err)
         res.send(`Запись с id ${req.body.id} не найдена!`);
      else if (results.key === req.body.password) {
         Object.keys(req.body).forEach((prop) => {    //обновить все переданные параметры
            if (typeof req.body[prop] !== 'undefined')
               results[prop] = req.body[prop];
         });

         results.save((err) => {
            if (err) {  // вывод сообщения об ошибке
               const err_message = require('./../errorOutput')(Object.keys(err.errors)[0], Object.values(err.errors)[0].message);
               res.status(200).send(err_message);
            }
            else {
               res.send(`Запись ${results.title} успешно обновлена!`)
            }
         });
      }
      else
         res.send(`Неверный пароль!`);
   });
}