const fs = require('fs');
const mongoose = require('mongoose');
const user = require('./../db/Schma');

module.exports = async function(app){  
    app.post('/add', require('./add'));
    app.post('/edit', require('./edit'));
    app.post('/adminEdit', require('./adminEdit'));
    app.post('/adminApprove', require('./adminApprove'));
    app.post('/del',require('./del'));
    app.get('/all',require('./all'));
    app.get('/allTrue',require('./allTrue'));
    app.get('/allFalse',require('./allFalse'));
    app.get('/archiv',require('./archiv'));
};